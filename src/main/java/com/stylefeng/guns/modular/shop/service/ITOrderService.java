package com.stylefeng.guns.modular.shop.service;

import com.stylefeng.guns.persistence.shop.model.TOrder;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
public interface ITOrderService extends IService<TOrder> {

    TOrder insertOrder(String[] cartIds, Long addressid, Long paymentid, String usercontent);

    TOrder insertWapOrder(Long productId, Long addressid, Long paymentid, String usercontent, Long id, String username);
}
